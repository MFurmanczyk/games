#include "Enemy.h"

void Enemy::MoveRight()
{
	this->SetPosition(GetPosition().x + velocity, GetPosition().y);
}

void Enemy::MoveLeft()
{
	this->SetPosition(GetPosition().x - velocity, GetPosition().y);
}

void Enemy::MoveDown()
{
	this->SetPosition(GetPosition().x, GetPosition().y + velocity * .25f);
}

void Enemy::Move(screenSize, bool _pDirection)
{
	if (_pDirection)
	{
		MoveRight();
		MoveDown();
	}
	else
	{
		MoveLeft();
		MoveDown();
	}
}

void Enemy::Shot()
{
	if (!(rand() % 1000) && !bulletShot)
	{
		bulletShot = new EnemyBullet(20, 5, GetPosition());
	}
	if (bulletShot)
	{
		bulletShot->Move(screenSize(win->getSize().x, win->getSize().y), 1);
		bulletShot->Draw(*win);
		if (bulletShot->GetPosition().y == win->getSize().y)
		{
			delete bulletShot;
			bulletShot = nullptr;
		}
	}
}