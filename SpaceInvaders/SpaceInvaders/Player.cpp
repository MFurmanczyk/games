#include "Player.h"

void Player::Move(screenSize screen, bool)
{
	
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && GetPosition().x > this->GetSize().x / 2)
		{
			SetPosition(GetPosition().x - this->GetVelocity(), GetPosition().y);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && GetPosition().x < screen.screenWidth - this->GetSize().x / 2)
		{
			SetPosition(GetPosition().x + this->GetVelocity(), GetPosition().y);
		}
}

void Player::Shot()
{

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && !bulletShot)
	{
		{

			if (bulletType == eBullet1)
			{
				bulletShot = new Bullet1(5, 10, GetPosition());
			}
			else if (bulletType == eBullet2)
			{
				bulletShot = new Bullet2(5, 20, GetPosition());
			}
			else if (bulletType == eBullet3)
			{
				bulletShot = new Bullet3(10, 10, GetPosition());
			}
		}
	}
	if (bulletShot)
	{
		bulletShot->Move(screenSize(win->getSize().x, win->getSize().y), 1);
		bulletShot->Draw(*win);
		if (bulletShot->GetPosition().y == 0)
		{
			delete bulletShot;
			bulletShot = nullptr;
		}
	}
}

