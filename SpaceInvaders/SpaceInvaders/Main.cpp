#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Player.h"
#include "Enemy.h"
#include "Board.h"
#include "ScreenSize.h"
#include <vector>
#include <ctime>
#include <iostream>
#include <windows.h>
#include <sstream>

sf::Text GenerateText(std::string _pText, sf::Font& _pFont, screenSize& _pScreen);

int main()
{
	int frameLimit = 60;
	screenSize screen(1000, 800);
	srand(time(NULL));
	sf::RenderWindow window{ sf::VideoMode{screen.screenWidth, screen.screenHeight}, "Space Invaders 1.0.0" };
	window.setFramerateLimit(frameLimit);
	sf::Event event;
	Board board(screen);
	sf::Font font;
	font.loadFromFile("arial.ttf");

	sf::Text textwon = GenerateText("Game won!", font, screen);
	sf::Text textlost = GenerateText("Game over!", font, screen);

	while (window.isOpen())
	{

		sf::Event event;
		window.clear(sf::Color::Black);
		window.pollEvent(event);
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		if (!board.IsGameWon() && !board.IsGameLost())
		{
			board.Play(window);
		}
		else if (board.IsGameWon())
		{
			window.draw(textwon);
		}
		else if (board.IsGameLost())
		{
			window.draw(textlost);
		}
		window.display();
	}
	return 0;
}

sf::Text GenerateText(std::string _pText, sf::Font& _pFont, screenSize& _pScreen)
{
	sf::Text text(_pText, _pFont);
	text.setCharacterSize(50);
	text.setStyle(sf::Text::Bold);
	text.setFillColor(sf::Color::Red);
	text.setPosition(_pScreen.screenHeight / 2, _pScreen.screenWidth / 2);
	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.width / 2, textRect.height / 2);
	text.setPosition(sf::Vector2f(_pScreen.screenWidth / 2.0f, _pScreen.screenHeight / 2.0f));
	return text;
}
