#include "GraphicObject.h"

GraphicObject::GraphicObject()
{
	if (!texture.loadFromFile("PlayerTexture.png"))
		throw "No texture found \n";
	sprite.setTexture(texture);
	sprite.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	health = 100;
	velocity = 3;
	SetPosition(sf::Vector2f(0.f, 0.f));
	bulletType = eBullet1;
}

GraphicObject::GraphicObject(int _pParam, float _pVelocity, sf::Vector2f _pPosition, std::string _texName)
{
	if (!texture.loadFromFile(_texName))
		throw "No texture found \n";
	sprite.setTexture(texture);
	sprite.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	health = _pParam;
	damage = _pParam;
	velocity = _pVelocity;
	bulletType = eBullet1;
	SetPosition(_pPosition);
}

void GraphicObject::Draw(sf::RenderWindow & window)
{
	window.draw(sprite);
	win = &window;
}

void GraphicObject::SetPosition(sf::Vector2f _position)
{
	sprite.setPosition(_position.x, _position.y);
}

void GraphicObject::SetPosition(float _xTransform, float _yTransform)
{
	sprite.setPosition(_xTransform, _yTransform);
}

sf::Vector2f GraphicObject::GetPosition() const
{
	return this->sprite.getPosition();
}

sf::Vector2u GraphicObject::GetSize() const
{
	return this->sprite.getTexture()->getSize();
}

int GraphicObject::GetHealth() const
{
	return this->health;
}

unsigned GraphicObject::GetDamage() const
{
	return this->damage;
}

unsigned GraphicObject::GetVelocity() const
{
	return this->velocity;
}

void GraphicObject::SetHealth(unsigned _pHit)
{
	this->health -= _pHit;
}

void GraphicObject::SetBulletType()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
	{
		this->bulletType = eBullet1;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
	{
		this->bulletType = eBullet2;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
	{
		this->bulletType = eBullet3;
	}
}
