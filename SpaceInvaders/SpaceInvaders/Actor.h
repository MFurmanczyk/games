#pragma once
#include "GraphicObject.h"
#include "Bullet.h"
#include "BulletType.h"
#include "ScreenSize.h"
#include <ctime>

///Base class for movable player and NPCs
class Actor : public GraphicObject
{
public:
	//public variables
	Bullet* bulletShot = nullptr;

	//contructors & destructors
	Actor() : GraphicObject() {};
	Actor(int _pHealth, float _pVelocity, sf::Vector2f _pPosition, std::string _texName) : GraphicObject(_pHealth,  _pVelocity, _pPosition, _texName) {};
	~Actor() { bulletShot = nullptr; };
};