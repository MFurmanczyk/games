#pragma once
#include <SFML/Graphics.hpp>
#include "BulletType.h"
#include "ScreenSize.h"


class GraphicObject
{
protected:
	sf::Sprite sprite;
	sf::Texture texture;
	sf::RenderWindow* win;
	BulletType bulletType;
	int health;
	unsigned damage;
	float velocity;
public:
	//constuctors & destructors

	GraphicObject();
	GraphicObject(int _pParam, float _pVelocity, sf::Vector2f _pPosition, std::string _texName); ///first parameter indicates on health or damage


	//Setters

	virtual void Draw(sf::RenderWindow&); ///draws object on screen
	virtual void SetPosition(sf::Vector2f); ///sets object's position on screen
	virtual void SetPosition(float _xTransform, float); ///sets object's position on screen
	virtual void SetHealth(unsigned); ///Modifying object's health
	virtual void SetBulletType(); ///sets bullet type


	//Constant methods

	sf::Vector2f GetPosition() const; ///returns object's position on screen
	sf::Vector2u GetSize() const; ///Returns texture size in pixels
	int GetHealth() const; ///returns health value
	unsigned GetDamage() const; ///returns health value
	unsigned GetVelocity() const; ///returns health value


	//pure virtual methods

	virtual void Shot() = 0;
	virtual void Move(screenSize, bool) = 0;
};