#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Player.h"
#include "Enemy.h"
#include "ScreenSize.h"
#include <vector>
#include <ctime>
#include <sstream>

class Board final
{
protected:
	bool direction = true;
	bool gameWon = false; //indicates if game is won. true if won
	bool gameLost = false; //iondicates if game is lost. true if lost
	int points = 0;
	std::vector<Actor*> enemies;
	Actor* player;
	screenSize screen;
	std::string pointsCounter;
	std::string healthCounter;
	sf::Font font;
	sf::Text pointsText;
	sf::Text healthText;

public:
	Board(screenSize&);
	bool IsGameWon() const;
	bool IsGameLost() const;
	void Play(sf::RenderWindow&);
	void DrawPoints(sf::RenderWindow&);
	void CollisionMappingEnemies();
	void SetPointsText();
	void SetHealthText();
	void CollisionMappingPlayer();
	int GetPoints() const;
};