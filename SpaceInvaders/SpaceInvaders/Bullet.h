#pragma once
#include "GraphicObject.h"
#include "ScreenSize.h"

class Bullet : public GraphicObject
{
public:
	Bullet() : GraphicObject() {};
	Bullet(int _pDmg, unsigned _pVel, sf::Vector2f _pPos, std::string _texName) : GraphicObject(_pDmg, _pVel, _pPos, _texName) {};
	virtual void Move(screenSize, bool) override;
	virtual void Shot() override {};
};

class Bullet1 final : public Bullet
{
public:
	Bullet1() : Bullet() {};
	Bullet1(int _pDmg, unsigned _pVel, sf::Vector2f _pPos) : Bullet(_pDmg, _pVel, _pPos, "Bullet1Tex.png") {};
};

class Bullet2 final : public Bullet
{
public:
	Bullet2() : Bullet() {};
	Bullet2(int _pDmg, unsigned _pVel, sf::Vector2f _pPos) : Bullet(_pDmg, _pVel, _pPos, "Bullet2Tex.png") {};
};

class Bullet3 final : public Bullet
{
public:
	Bullet3() : Bullet() {};
	Bullet3(int _pDmg, unsigned _pVel, sf::Vector2f _pPos) : Bullet(_pDmg, _pVel, _pPos, "Bullet3Tex.png") {};
};

class EnemyBullet final : public Bullet
{
public:
	EnemyBullet() : Bullet() {};
	EnemyBullet(int _pDmg, unsigned _pVel, sf::Vector2f _pPos) : Bullet(_pDmg, _pVel, _pPos, "Bullet1Tex.png") {};
	virtual void Move(screenSize, bool) override;
};