#include "Board.h"
#include <iostream>

Board::Board(screenSize& _pScreen)
{
	std::string textureArray[2] = { "OponentTexture1.png", "OponentTexture2.png" };
	player = new Player(100, 4, sf::Vector2f(_pScreen.screenWidth / 2, _pScreen.screenHeight * 0.90f));
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			int textureIndex = rand() % 2;
			Actor * enemy = new Enemy(10, .5f, sf::Vector2f(800 / 10 * j + 200, 300.0 / 4 * i + 20), textureArray[textureIndex]); //TODO Random assigned textures
			this->enemies.push_back(enemy);
		}
	}
	points = 0;
	screen = _pScreen;
	font.loadFromFile("arial.ttf");
	SetPointsText();
	SetHealthText();
}

bool Board::IsGameWon() const
{
	return gameWon;
}

bool Board::IsGameLost() const
{
	return gameLost;
}

void Board::Play(sf::RenderWindow& window)
{
	for (int i = 0; i < enemies.size(); i++)
	{
		enemies[i]->Draw(window);
		if (enemies[i]->GetPosition().y >= screen.screenHeight * .75f)
		{
			gameLost = true;
		}
		enemies[i]->Shot();
		if (enemies[i]->GetPosition().x + enemies[i]->GetSize().x / 2 > screen.screenWidth)
		{
			direction = false;
		}
		if (enemies[i]->GetPosition().x - enemies[i]->GetSize().x / 2 == 0 )
		{
			direction = true;
		}
		enemies[i]->Move(screen, direction);
	}
	player->Draw(window);
	player->Move(screen, 1);
	player->Shot();
	player->SetBulletType();
	CollisionMappingEnemies();
	CollisionMappingPlayer();
	DrawPoints(window);
	if (enemies.empty())
	{
		gameWon = true;
	}
}

void Board::DrawPoints(sf::RenderWindow& window)
{
	pointsCounter = "Points: " + std::to_string(GetPoints());
	healthCounter = "Health: " + std::to_string(player->GetHealth()); //can be done with stringsteam
	pointsText.setString(pointsCounter);
	healthText.setString(healthCounter);
	window.draw(pointsText);
	window.draw(healthText);
}

void Board::CollisionMappingEnemies()
{
	if (player->bulletShot)
	{
		for (auto it = enemies.begin(); it != enemies.end(); it++)
		{
			if (player->bulletShot->GetPosition().x > ((*it)->GetPosition().x - ((*it)->GetSize().x / 2)) &&
				player->bulletShot->GetPosition().x < ((*it)->GetPosition().x + ((*it)->GetSize().x / 2)) &&
				player->bulletShot->GetPosition().y > ((*it)->GetPosition().y - ((*it)->GetSize().y / 2)) &&
				player->bulletShot->GetPosition().y < ((*it)->GetPosition().y + ((*it)->GetSize().y / 2)))
			{
				(*it)->SetHealth(player->bulletShot->GetDamage());
				if ((*it)->GetHealth() <= 0)
				{
					points++;
					delete (*it);
					(*it) = nullptr;
					enemies.erase(it);
				}
				delete player->bulletShot;
				player->bulletShot = nullptr;
				break;
			} 
		}
	}
}

void Board::SetPointsText()
{
	pointsText.setFont(font);
	pointsText.setCharacterSize(20);
	pointsText.setStyle(sf::Text::Bold);
	pointsText.setFillColor(sf::Color::Red);
	pointsText.setPosition(screen.screenHeight / 2, screen.screenWidth / 2);
	sf::FloatRect textRect = pointsText.getLocalBounds();
	pointsText.setOrigin(textRect.width, textRect.height);
	pointsText.setPosition(sf::Vector2f(screen.screenWidth * .1f, screen.screenHeight * .95f));
}

void Board::SetHealthText()
{
	healthText.setFont(font);
	healthText.setCharacterSize(20);
	healthText.setStyle(sf::Text::Bold);
	healthText.setFillColor(sf::Color::Red);
	healthText.setPosition(screen.screenHeight / 2, screen.screenWidth / 2);
	sf::FloatRect textRect = healthText.getLocalBounds();
	healthText.setOrigin(textRect.width, textRect.height);
	healthText.setPosition(sf::Vector2f(screen.screenWidth * .8f, screen.screenHeight * .95f));
}

void Board::CollisionMappingPlayer()
{

		for (auto it = enemies.begin(); it != enemies.end(); it++)
		{
			if ((*it)->bulletShot)
			{
				if ((*it)->bulletShot->GetPosition().x < player->GetPosition().x + player->GetSize().x / 2 &&
					(*it)->bulletShot->GetPosition().x > player->GetPosition().x - player->GetSize().x / 2 &&
					(*it)->bulletShot->GetPosition().y < player->GetPosition().y + player->GetSize().y / 2 &&
					(*it)->bulletShot->GetPosition().y > player->GetPosition().y - player->GetSize().y / 2)
				{
					player->SetHealth((*it)->bulletShot->GetDamage());
					if ((player->GetHealth() <= 0 ))
					{
						gameLost = true;
					}
					delete (*it)->bulletShot;
					(*it)->bulletShot = nullptr;
					break;
				}
			}
		}
	
}

int Board::GetPoints() const
{
	return points;
}
