#pragma once
#include "Actor.h"

class Player final : public Actor
{
public:

	Player() : Actor() {};
	Player(int _pHealth, float _pVelocity, sf::Vector2f _pPosition) : Actor(_pHealth, _pVelocity, _pPosition, "PlayerTexture.png") {};
	void Move(screenSize, bool) override;
	void Shot() override; 
};