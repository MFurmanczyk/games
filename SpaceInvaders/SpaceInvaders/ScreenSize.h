#pragma once

struct screenSize
{
public:
	unsigned screenWidth;
	unsigned screenHeight;

	screenSize() : screenWidth(800u), screenHeight(600u) {};
	screenSize(unsigned _scrW, unsigned _scrH) : screenWidth(_scrW), screenHeight(_scrH) {};
};