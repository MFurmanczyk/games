#pragma once
#include "GraphicObject.h"
#include "Bullet.h"
#include "BulletType.h"
#include "ScreenSize.h"
#include <ctime>
#include "Actor.h"

class Enemy final : public Actor
{
protected:
	void MoveRight();
	void MoveLeft();
	void MoveDown();
public:
	Enemy() :Actor() {};
	Enemy(int _pHealth, float _pVelocity, sf::Vector2f _pPosition, std::string _texName) : Actor(_pHealth, _pVelocity, _pPosition, _texName) {};

	virtual void Move(screenSize, bool) override;
	virtual void Shot() override;
};